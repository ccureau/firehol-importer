package com.flowroute.demo.ccureau.scheduled;

import com.flowroute.demo.ccureau.utility.FireholUpdate;
import com.flowroute.demo.ccureau.configuration.GitConf;
import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.service.AdminService;
import io.micronaut.scheduling.annotation.Scheduled;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.micronaut.SchedulerLock;

import javax.inject.Singleton;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static net.javacrumbs.shedlock.core.LockAssert.assertLocked;

@Singleton
@Slf4j
public class MongoUpdate {

    private final FireholUpdate fireholUpdate;
    private final AdminService adminService;
    private final MongoConf mongoConf;
    private final GitConf gitConf;

    public MongoUpdate(FireholUpdate fireholUpdate, AdminService adminService, MongoConf mongoConf, GitConf gitConf) {
        this.fireholUpdate = fireholUpdate;
        this.adminService = adminService;
        this.mongoConf = mongoConf;
        this.gitConf = gitConf;
    }
    
    @Scheduled(fixedDelay = "1d")
    @SchedulerLock(name = "fireholUpdate")
    public void fireholUpdate() {
        try {
            assertLocked();
            Date lastUpdate = adminService.getLastUpdate();
            if (lastUpdate == null || elapsedSeconds(lastUpdate) >= Long.parseLong(mongoConf.getCacheExpireTime())) {

                fireholUpdate.update(gitConf.getRepository());
            }
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    /**
     * returns the number of seconds elapsed since the passed-in date
     */
    private long elapsedSeconds(Date d) {
        TimeUnit time = TimeUnit.SECONDS;
        long diff = new Date().getTime() - d.getTime();
        return time.convert(diff, TimeUnit.MILLISECONDS);
    }
}
