package com.flowroute.demo.ccureau.factory;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.mongodb.client.MongoClient;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.mongo.MongoLockProvider;

@Factory
public class LockFactory {
    private final MongoClient mongoClient;
    private final MongoConf mongoConf;

    public LockFactory(MongoClient mongoClient, MongoConf mongoConf) {
        this.mongoClient = mongoClient;
        this.mongoConf = mongoConf;
    }

    @Bean
    public LockProvider lockProvider() {
        return new MongoLockProvider(mongoClient.getDatabase(mongoConf.getDatabaseName()));
    }
}
