package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import lombok.Getter;
import lombok.Setter;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;

@Introspected
public class Admin {
    @Getter
    @Setter
    private ObjectId id;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "type")
    private String type;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "lastUpdate")
    private Date lastUpdate;
}