package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import lombok.Getter;
import lombok.Setter;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Objects;

@Introspected
public class FireholList {

    @Getter
    @Setter
    private ObjectId id;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "ipAddress")
    private String ipAddress;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "firstOctet")
    private String firstOctet;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "maskBits")
    private String maskBits;

    @NonNull
    @Getter
    @Setter
    @BsonProperty(value = "listName")
    private String listName;

    public boolean equals(FireholList item) {
        return this.listName.equals(item.getListName())
                && this.ipAddress.equals(item.getIpAddress())
                && this.firstOctet.equals(item.getFirstOctet())
                && this.maskBits.equals(item.getMaskBits());
    }

    public int hash() {
        return Objects.hash(this.ipAddress, this.firstOctet, this.maskBits, this.listName);
    }
}
