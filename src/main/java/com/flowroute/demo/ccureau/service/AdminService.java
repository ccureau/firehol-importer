package com.flowroute.demo.ccureau.service;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.model.Admin;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

@Singleton
@Slf4j
@ExecuteOn(TaskExecutors.IO)
public class AdminService {

    private static final String COLLECTION_NAME = "admin";
    private final MongoClient mongoClient;
    private final MongoConf mongoConf;

    public AdminService(MongoClient mongoClient, MongoConf mongoConf) {
        this.mongoClient = mongoClient;
        this.mongoConf = mongoConf;
    }

    public Date getLastUpdate() {

        FindIterable<Admin> iterable = getCollection().find(eq("type", "lastUpdate"));
        Iterable<Admin> list = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        if (list.iterator().hasNext()) {
            return list.iterator().next().getLastUpdate();
        } else {
            // Document doesn't exist for some reason. Create it, and return null
            // Ths triggers an update.
            Admin lastUpdate = new Admin();
            lastUpdate.setType("lastUpdate");
            lastUpdate.setLastUpdate(null);
            getCollection().insertOne(lastUpdate);
        }
        return null;
    }

    public void setLastUpdate(Date date) {
        getCollection().findOneAndUpdate(eq("type", "lastUpdate"),
                set("lastUpdate", date));
    }

    private MongoCollection<Admin> getCollection() {
        return mongoClient
                .getDatabase(mongoConf.getDatabaseName())
                .getCollection(COLLECTION_NAME, Admin.class);
    }
}
