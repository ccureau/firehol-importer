package com.flowroute.demo.ccureau.utility;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.model.FireholList;
import com.flowroute.demo.ccureau.service.AdminService;
import com.flowroute.demo.ccureau.service.FireholListService;
import com.mongodb.client.MongoClient;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.RefSpec;

import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Singleton
@Slf4j
public class FireholUpdate {

    private final static String CLONE_DIR = "/tmp/clone/";

    private final MongoConf mongoConf;
    private final MongoClient mongoClient;
    private final FireholListService fireholListService;
    private final AdminService adminService;

    public FireholUpdate(MongoConf mongoConf, MongoClient mongoClient, FireholListService fireholListService,
                         AdminService adminService) {
        this.mongoConf = mongoConf;
        this.mongoClient = mongoClient;
        this.fireholListService = fireholListService;
        this.adminService = adminService;
    }

    /*
     * For each filename, we build a temporary document so we don't interfere with existing data.
     * When the document is complete, we replace the entire original document.
     */
    @ExecuteOn(TaskExecutors.IO)
    public void update(String gitUrl) throws GitAPIException, IOException {
        log.info("Beginning import");
        try {
            pull(gitUrl);
        } catch (Exception e) {
            deleteDirectory();
            clone(gitUrl);
        }
        List<String> fileNames = getFilesMatching(CLONE_DIR);
        for (String fileName : fileNames) {
            log.info("importing " + fileName);
            List<FireholList> entries = importFile(fileName);
            if (! entries.isEmpty()) {
                fireholListService.updateCollection(entries, fileName);
            }
        }
        adminService.setLastUpdate(new Date());
        log.info("Import complete");
    }

    private void pull(String gitUrl) throws GitAPIException, NotDirectoryException {
        Git git;
        File path = new File(CLONE_DIR);
        if (! path.exists()) {
            log.error("Directory does not exist");
            throw new NotDirectoryException("Directory does not exist");
        }
        log.info("Fetching updated data from repository");

        git = Git.init()
                .setDirectory(new File(CLONE_DIR))
                .call();

        git.fetch()
                .setRemote(gitUrl)
                .setRefSpecs(new RefSpec("+refs/heads/master:refs/remotes/origin/master"))
                .call();
    }

    private void clone(String gitUrl) throws GitAPIException {
        new File(CLONE_DIR).mkdirs();
        log.info("Cloning from repository");
        Git.cloneRepository()
                .setURI(gitUrl)
                .setDirectory(new File(CLONE_DIR))
                .call();
    }

    private static void deleteDirectory() {
        Path path = Paths.get(CLONE_DIR);
        try (Stream<Path> walk = Files.walk(path)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(FireholUpdate::deletePath);
        } catch (Exception e) {
            //
        }
    }

    private static void deletePath(Path path) {
        try {
            Files.delete(path);
        } catch (Exception e) {
            log.error("Unable to delete path " + path + ": " + e);
        }
    }

    public List<String> getFilesMatching(String directory) throws IOException {
        List<String> result = new ArrayList<>();
        try (Stream<Path> walkStream = Files.walk(Paths.get(directory))) {
            walkStream.filter(p -> p.toFile().isFile()).forEach(f -> {
                if (f.toString().endsWith("ipset") || f.toString().endsWith("netset")) {
                    result.add(f.toString().replace(directory, ""));
                }
            });
        }
        return result;
    }

    public List<FireholList> importFile(String fileName) {
        List<FireholList> list = new ArrayList<>();
        File f = new File(fixupPath() + fileName);
        try (Stream linesStream = Files.lines(f.toPath())) {
            linesStream.forEach(line -> {
                // Comments in line begin with #
                String l = line.toString();
                if (! l.startsWith("#")) {
                    FireholList entry = new FireholList();
                    entry.setListName(fileName);
                    // If we have a mask, pull it out now
                    if (l.contains("/")) {
                        int index = l.indexOf("/");
                        entry.setIpAddress(l.substring(0, index));
                        entry.setMaskBits(l.substring(index + 1));
                    } else {
                        entry.setIpAddress(l);
                        entry.setMaskBits("32");
                    }
                    String[] tokens = l.split("\\.");
                    entry.setFirstOctet(tokens[0]);
                    list.add(entry);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private String fixupPath() {
        if (! FireholUpdate.CLONE_DIR.endsWith("/")) {
            return FireholUpdate.CLONE_DIR + "/";
        }
        return FireholUpdate.CLONE_DIR;
    }
}
