package com.flowroute.demo.ccureau.configuration;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.annotation.NonNull;

import javax.validation.constraints.NotNull;

@ConfigurationProperties("git")
public class GitConfiguration implements GitConf {
    @NonNull
    @NotNull
    private String repository;

    @Override
    public String getRepository() {
        return repository;
    }

    public void setRepository(@NonNull String repository) {
        this.repository = repository;
    }
}
