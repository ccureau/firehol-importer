package com.flowroute.demo.ccureau.configuration;

import io.micronaut.core.annotation.NonNull;

public interface GitConf {
    @NonNull
    String getRepository();
}
