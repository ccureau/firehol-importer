package com.flowroute.demo.ccureau.configuration;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.annotation.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@ConfigurationProperties("docdb")
public class MongoConfiguration implements MongoConf {
    @Setter
    @NonNull
    @NotNull
    private String url;

    @Setter
    @NonNull
    @NotNull
    private String databaseName;

    @Setter
    @NonNull
    @NotNull
    private String username;

    @Setter
    @NonNull
    @NotNull
    private String password;

    @Setter
    @NonNull
    @NotNull
    private String cacheCollectionName;

    @Setter
    @NonNull
    @NotNull
    private String listCollectionName;

    @Setter
    @NonNull
    @NotNull
    private String overrideCollectionName;

    @Setter
    @NonNull
    @NotNull
    private String cacheExpireTime;

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    @Override
    public String getUsername() { return username; }

    @Override
    public String getPassword() { return password; }

    @Override
    public String getCacheCollectionName() {
        return cacheCollectionName;
    }

    @Override
    public String getListCollectionName() {
        return listCollectionName;
    }

    @Override
    public String getOverrideCollectionName() {
        return overrideCollectionName;
    }

    @Override
    public String getCacheExpireTime() {
        return cacheExpireTime;
    }
}
