package com.flowroute.demo.ccureau.configuration;

import io.micronaut.core.annotation.NonNull;

public interface MongoConf {
    @NonNull
    String getUrl();

    @NonNull
    String getDatabaseName();

    @NonNull
    String getUsername();

    @NonNull
    String getPassword();

    @NonNull
    String getCacheCollectionName();

    @NonNull
    String getListCollectionName();

    @NonNull
    String getOverrideCollectionName();

    @NonNull
    String getCacheExpireTime();
}
